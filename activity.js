/*
The questions are as follows:
a. What directive is used by Node.js in loading the modules it needs?
answer:
    - http

b. What Node.js module contains a method for server creation?
answer:
    - createServer()

c. What is the method of the http object responsible for creating a server using Node.js?
answer:
    - http.createServer()

d. What method of the response object allows us to set status codes and content types?
answers:
    - res.writeHead()

e. Where will console.log() output its contents when run in Node.js?
answer:
    - terminal

f. What property of the request object contains the address' endpoint?
answer:
    - string
*/

const http = require ('http');
const port = 3000

const server = http.createServer(function(req, res) {

    if (req.url == '/login'){
        res.writeHead(200, {'content-Type': 'text/plain'});
        res.end("Welcome to the login page");
    } else if (req.url == '/register'){
        res.writeHead(200, {'content-Type': 'text/plain'});
        res.end("Welcome to the register page");
    } else {
        res.writeHead(404, {'content-Type': 'text/plain'});

        res.end("I'm sorry the page you are looking for cannot be found");
    }
  
});

server.listen(port);
console.log(`Server is now running at port: ${port}`);

